Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.

* [Repository](#repository)
* [Translations](#translations)

## Repository

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

#### Rules: 

* Only Java 8 should be used. All the functions in the latest version of Java 8 can be used.
* Make sure the utility works on different versions.
* Use method and variable names that make sense and are related to the context.
* Do not attempt to support older versions than 1.8 even if it can be fixed with a single line.

## Translations

We are always looking to expand and improve localization. If you want to help us translate Player Plot into your native language, join the [Player Plot Crowdin](https://crowdin.com/project/player-plot).